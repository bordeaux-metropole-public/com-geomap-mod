<?php

namespace Drupal\geofield_map_geosoftware\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield_map\Plugin\views\style\GeofieldGoogleMapViewStyle;
use Drupal\geofield_map_ext\GeofieldMapFieldInterface;
use Drupal\geofield_map_geosoftware\GeoSoftwareGeofieldMapFieldTrait;

/**
 * Style plugin to render a View output as a Leaflet map.
 *
 * @ingroup views_style_plugins
 *
 * Attributes set below end up in the $this->definition[] array.
 *
 * @ViewsStyle(
 *   id = "geofield_geosoftware_map",
 *   title = @Translation("GEO Software Map"),
 *   help = @Translation("Displays a View as a Geofield GEO Software Map."),
 *   display_types = {"normal"},
 *   theme = "geofield-geosoftware-map"
 * )
 */
class GeofieldGeoSoftwareMapViewStyle extends GeofieldGoogleMapViewStyle implements GeofieldMapFieldInterface {

  use GeoSoftwareGeofieldMapFieldTrait;

  /**
   * Get the GEO Software Default Settings.
   *
   * @return array
   *   The default settings.
   */
  public static function getDefaultSettings() {
    return self::getGeoSoftwareDefaultSettings() + parent::getDefaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldName() {
    return $this->fieldDefinition->getName();
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Renders the View.
   */
  public function render() {
    $element = parent::render();

    $element['#attached']['library'] = array_diff($element['#attached']['library'] ?? [],
      [
        'geofield_map/geofield_google_map',
        'geofield_map/overlappingmarkerspiderfier',
      ]);
    $element['#attached']['library'][] = 'geofield_map_geosoftware/geofield_geosoftware_map';

    $element['#theme'] = 'geofield_geosoftware_map';

    $drupalSettings = $element['#attached']['drupalSettings']['geofield_google_map'];
    $element['#attached']['drupalSettings']['geofield_geosoftware_map'] = $drupalSettings;
    unset($element['#attached']['drupalSettings']['geofield_google_map']);

    return $element;
  }

}
