<?php

namespace Drupal\geofield_map_geosoftware;

use Drupal\Core\Url;
use Drupal\geofield_map_ext\GeofieldMapFieldInterface;

/**
 * Class GeoSoftwareGeofieldMapFieldUtils.
 *
 * Provide common functions for Geofield GEO Software Map fields.
 *
 * @package Drupal\geofield_map_geosoftware
 */
class GeoSoftwareGeofieldMapFieldUtils {

  /**
   * Set Map GEO Settings Element.
   *
   * @param \Drupal\geofield_map_ext\GeofieldMapFieldInterface $field
   * @param array $settings
   * @param array $elements
   * @param bool $use_map_library
   */
  public static function setGeoSettingsElement(
    GeofieldMapFieldInterface &$field,
  array $settings,
  array &$elements,
  bool $use_map_library = FALSE) {
    $elements['geo_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('GEO Settings'),
    ];
    if ($use_map_library) {
      $elements['geo_settings']['#states'] = [
        'visible' => [
          ':input[name="fields[' . $field->getFieldName() . '][settings_edit_form][settings][map_library]"]' => ['value' => 'geo'],
        ],
      ];
    }
    $geo_settings = $settings['geo_settings'] ?? [];
    $elements['geo_settings']['application_url'] = [
      '#type' => 'textfield',
      '#title' => t('GEO Application URL'),
      '#default_value' => $geo_settings['application_url'] ?? '',
      '#description' => t('URL of your GEO application. The application must contain the "GEO API v2" module.'),
      '#placeholder' => t('https://geo.com/adws/app/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx/'),
      '#required' => !$use_map_library,
    ];
    $elements['geo_settings']['api_version'] = [
      '#type' => 'textfield',
      '#title' => t('GEO API Version'),
      '#default_value' => $geo_settings['api_version'] ?? '',
      '#description' => t('Check that the GEO API version is compatible with your GEO version.<br>@geo_api_version_link.', [
        '@geo_api_version_link' => $field->getLink()->generate(t('Check the compatibility matrix in the GEO API documentation'), Url::fromUri('https://docgeoapi.business-geografic.com/en/guide/1-Overview/requirements', [
          'absolute' => TRUE,
          'attributes' => ['target' => '_blank'],
        ])),
      ]),
      '#required' => !$use_map_library,
      '#placeholder' => t('v2.2.2'),
    ];
  }

  /**
   * Set Map GEO Settings Summary Element.
   *
   * @param array
   *   GEO settings.
   */
  public static function setGeoSettingsSummaryElement(array $geo_settings) {
    $geo_settings_value = t("<strong>Application URL:</strong> @application_url", [
      '@application_url' => $geo_settings['application_url'],
    ]);
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $geo_settings_value,
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => '',
      ],
    ];
  }

}
